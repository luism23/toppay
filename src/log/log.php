<?php
if(TOPPAY_LOG){
    function add_TOPPAY_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'TOPPAY_LOG',
                'title' => 'TOPPAY_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=TOPPAY_LOG'
            )
        );
    }

    function TOPPAY_LOG_option_page()
    {
        add_options_page(
            'Log Toppay',
            'Log Toppay',
            'manage_options',
            'TOPPAY_LOG',
            'TOPPAY_LOG_page'
        );
    }

    function TOPPAY_LOG_page()
    {
        $log = TOPPAY_get_optionPage("TOPPAY_LOG");
        $log = array_reverse($log);
        ?>
        <script>
            const log = <?=json_encode($log)?>;
        </script>
        <h1>
            Log
        </h1>
        <pre><?php var_dump($log);?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_TOPPAY_LOG_option_page', 100);

    add_action('admin_menu', 'TOPPAY_LOG_option_page');

}

function addTOPPAY_LOG($newLog)
{
    if(!TOPPAY_LOG){
        return;
    }
    TOPPAY_put_optionPage("TOPPAY_LOG",$newLog);
}