<?php

function TOPPAY_get_optionPage($id = "TOPPAY_optionPage")
{
    $TOPPAY_optionPage = get_option($id);
    if($TOPPAY_optionPage === false || $TOPPAY_optionPage == null || $TOPPAY_optionPage == ""){
        $TOPPAY_optionPage = "[]";
    }
    $TOPPAY_optionPage = json_decode($TOPPAY_optionPage,true);
    return $TOPPAY_optionPage;
}

function TOPPAY_put_optionPage($id,$newItem)
{
    $TOPPAY_optionPage = TOPPAY_get_optionPage($id);
    $TOPPAY_optionPage[] = array(
        "date" => date("c"),
        "data" => $newItem,
    );
    update_option($id,json_encode($TOPPAY_optionPage));
}