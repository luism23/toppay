<?php
/*
Plugin Name: TopPay Integration
Plugin URI: https://gitlab.com/luism23/toppay
Description: Integración de TopPay para procesar pagos en WooCommerce.
Author: Byte4Bit
Version: 1.0.8
Author URI: https://byte4bit.com/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/toppay',
	__FILE__,
	'toppay'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('develop');


function TOPPAY_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("TOPPAY_LOG", true);
define("TOPPAY_PATH", plugin_dir_path(__FILE__));
define("TOPPAY_URL", plugin_dir_url(__FILE__));

require_once TOPPAY_PATH . "src/_index.php";

// Verificar si WooCommerce está activo
if (function_exists('WC')) {
    add_action('plugins_loaded', 'toppay_init_gateway');
}

function toppay_init_gateway() {
    if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        // Agregar la integración de TopPay a WooCommerce
        function toppay_add_to_woocommerce_gateways($gateways) {
            $gateways[] = 'TopPay_WooCommerce_Gateway';
            return $gateways;
        }
        add_filter('woocommerce_payment_gateways', 'toppay_add_to_woocommerce_gateways');
        

        class TopPay_WooCommerce_Gateway extends WC_Payment_Gateway {
            public function __construct() {
                $this->id = 'toppay_gateway';
                $this->method_title = 'TopPay';
                $this->icon = ''; // Agrega aquí la URL del icono de TopPay
    
                $this->has_fields = true;
    
                $this->init_form_fields();
                $this->init_settings();
    
                $this->title = $this->get_option('title');
                $this->description = $this->get_option('description');
    
                add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            }
    
            public function init_form_fields() {
                $this->form_fields = array(
                    'enabled' => array(
                        'title'   => 'Habilitar/Deshabilitar',
                        'type'    => 'checkbox',
                        'label'   => 'Habilitar TopPay como opción de pago',
                        'default' => 'yes',
                    ),
                    'title' => array(
                        'title'       => 'Título',
                        'type'        => 'text',
                        'description' => 'Título que se mostrará durante el proceso de pago.',
                        'default'     => 'TopPay',
                    ),
                    'description' => array(
                        'title'       => 'Descripción',
                        'type'        => 'textarea',
                        'description' => 'Descripción que se mostrará durante el proceso de pago.',
                        'default'     => 'Paga con TopPay',
                    ),
                );
            }
    
            public function process_payment($order_id) {
                global $woocommerce;
    
                $order = new WC_Order($order_id);
    
                // Aquí debes implementar la lógica para enviar la solicitud al procesador de pagos TopPay y manejar la respuesta
    
                // Ejemplo: enviar una solicitud POST al endpoint de transacciones de TopPay
                $response = wp_safe_remote_post('https://production.toppaylatam.com/api/transactions', array(
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'Authorization' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtZXJjaGFudCI6ODEsImlhdCI6MTY4OTE4ODEyNH0.BNUGS9Pr9hSPxMBiibtA0ZBzXdGW6ZciUxWO4dzzWc4',
                    ),
                    'body' => json_encode(array(
                        // Datos de la transacción
                    )),
                ));
                
                if (!is_wp_error($response)) {
                    $response_body = wp_remote_retrieve_body($response);
                    $response_data = json_decode($response_body, true);
    
                    $error_message = $response->get_error_message();
                
                    if (isset($response_data['status']) && $response_data['status'] === 'success') {
                        // La transacción se completó con éxito
                        // Actualizar el estado del pedido y vaciar el carrito
                        $order->payment_complete();
                        $woocommerce->cart->empty_cart();
                        
                        // Redirigir al cliente a la página de agradecimiento
                        return array(
                            'result' => 'success',
                            'redirect' => $this->get_return_url($order),
                        );
                    } else {
                        // La transacción falló
                        // Puedes manejar el fallo de acuerdo a tus necesidades
                    }
                } else {
                    $error_message = $response->get_error_message();
                }
                
    
                // Manejar la respuesta y actualizar el estado del pedido en base a la respuesta de TopPay
    
                // Marcar el pedido como completado y vaciar el carrito
                $order->payment_complete();
                $woocommerce->cart->empty_cart();
    
                // Redirigir al cliente a la página de agradecimiento
                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order),
                );
            }
        }
    }
}

// Agregar la opción de configuración en la sección de pago de WooCommerce
function toppay_add_settings($settings) {
    $settings[] = array(
        'title' => 'Configuración de TopPay',
        'type' => 'title',
        'desc' => 'Configuración para el procesador de pagos TopPay',
        'id' => 'toppay_settings_title',
    );

    $settings[] = array(
        'title' => 'Habilitar TopPay',
        'desc' => 'Habilitar el procesador de pagos TopPay',
        'id' => 'toppay_enabled',
        'type' => 'checkbox',
        'default' => 'no',
    );

    // Agregar más campos de configuración según tus necesidades, como campos para ingresar las credenciales y opciones adicionales.

    $settings[] = array(
        'type' => 'sectionend',
        'id' => 'toppay_settings_sectionend',
    );

    return $settings;
}
add_filter('woocommerce_settings_api_sanitized_fields_toppay', 'toppay_add_settings');

if (function_exists('WC')) {
    $toppay_enabled = WC_Admin_Settings::get_option('toppay_enabled', 'no');

    if ($toppay_enabled === 'yes') {
        add_action('woocommerce_before_checkout_form', 'toppay_process_options');

        function toppay_process_options() {
            // Aquí realizas las acciones necesarias para configurar y utilizar TopPay
            // Por ejemplo, podrías mostrar un formulario para ingresar las credenciales de TopPay
            // y luego procesar esas credenciales cuando se envíe el formulario.
        }
    }
}




